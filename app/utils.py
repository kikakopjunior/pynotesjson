from colorama import Style, Fore, Back
import time
import os


color = {
    'black_light': Fore.LIGHTBLACK_EX,
    'white_light': Fore.LIGHTWHITE_EX,
    'red': Fore.LIGHTRED_EX,
    'green': Fore.LIGHTGREEN_EX,
    'yellow': Fore.LIGHTYELLOW_EX,
    'blue': Fore.LIGHTBLUE_EX,
    'black': Fore.BLACK,
    'white': Fore.WHITE
}

background = {
    'black_light': Back.LIGHTBLACK_EX,
    'white_light': Back.LIGHTWHITE_EX,
    'red': Back.LIGHTRED_EX,
    'green': Back.LIGHTGREEN_EX,
    'yellow': Back.LIGHTYELLOW_EX,
    'blue': Back.LIGHTBLUE_EX,
    'black': Back.BLACK,
    'white': Back.WHITE,
    'none': ''
}

style = {
    'normal': Style.NORMAL,
    'dim': Style.DIM,
    'bright': Style.BRIGHT
}


default_sytle_grid= {
    'color': 'black',
    'style': 'bright'
}


def width() -> int:
    return os.get_terminal_size().columns


def render_text(target:str, space_option:bool=True, **kwargs) -> str:
    str_render:str = ""
    space = " "
    for key, value in kwargs.items():
        if key == 'style' and value in style:
            str_render += style[value]
        elif key == 'background' and value in background:
            str_render += background[value]
        elif key == 'color' and value in color:
            str_render += color[value]
        else:
            raise ValueError(f"Option non valide {key}")
    str_render += f'{space}{target}{space}' if space_option else f'{target}'
    str_render += Style.RESET_ALL
    return str_render


def concat_text_args(*args, **kwargs):
    msg_complet = ""
    space = " "
    for idx, arg in enumerate(args):
        styles_text = kwargs.get(f'idx_{idx}')
        if styles_text and isinstance(styles_text, dict):
            text = render_text(arg, **styles_text)
            msg_complet += f"{text}{space}"
        else:
            msg_complet += arg 
    return msg_complet


def text_transform():
    def wrapper_wrapper(func):
        def wrapper(text, *args, **kwargs):
            text += concat_text_args(*args, **kwargs)
            return func(text, *args, **kwargs)
        return wrapper
    return wrapper_wrapper

def add_separator():
    def decorator(func):
        def wrapper(*args, **kwargs):
            print('-' * (width() - 5))
            result = func(*args, **kwargs)  # Appel de la fonction décorée
            print('-' * (width() - 5))
            return result
        return wrapper
    return decorator


@text_transform()
def render(text,*args, **kwargs):
    print(text)

@text_transform()
def enter(text, *args, **kwargs):
    return input(text)


def clear():
    if os.name == 'nt':
        return os.system('cls')
    else:
        return os.system('clear')
    
def grid(headers: list, tables: list):
    if not all(isinstance(header, str) for header in headers):
        raise TypeError('All headers must be strings')
    if not all(isinstance(row, list) for row in tables):
        raise TypeError("Each row in tables must be a list")

    if len(headers) != len(tables[0]):
        raise ValueError("The length of headers does not match the length of table rows")

    # Calculer la largeur maximale pour chaque colonne
    column_widths = [max(len(str(item)) for item in [header] + [row[idx] for row in tables]) for idx, header in enumerate(headers)]

    # Créer une ligne de séparation
    separator = '-----'.join('-' * width for width in column_widths)

    # Afficher les en-têtes
    header_row = ' '.join(render_text(header.ljust(width), **default_sytle_grid) for header, width in zip(headers, column_widths))
    print(separator)
    print(header_row)
    print(separator)

    # Afficher les lignes de table
    for row in tables:
        row_str = ' '.join(render_text(str(item).ljust(width), **default_sytle_grid) for item, width in zip(row, column_widths))
        print(row_str)
    print(separator)


def progress_bar(length=50, symbol='█', sleep_time=0.3):
    """
    Affiche une barre de progression.

    Args:
    length (int): Longueur de la barre de progression.
    symbol (str): Symbole utilisé pour la barre.
    sleep_time (float): Temps de pause entre chaque mise à jour.
    """
    for i in range(length + 1):
        percent = int((i / length) * 100)
        bar = symbol * i + '-' * (length - i)
        print(f"\r[{bar}] {percent}%", end="")
        time.sleep(sleep_time)
    print()  # Pour passer à la ligne suivante après la fin de la progression


def continue_loop() :
    return enter('', 'Press to continue ...', idx_0={"color": 'red', 'style': 'bright'})







