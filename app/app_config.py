import app.view as view
import app.utils as utils
import app.models as models
import app.notes_controller as notes_controller


class AppConfig:
    def __init__(self) -> None:
        self.commands = {
            'create': {
                'args': ['filename'],
                'defaults': {},
                'func': self.create_command
            },
            'listfiles': {
                'args': ['extension'],
                'defaults': {'extension': '*'},
                'func': self.listfiles_command
            },
            'select':{
                'args': ['filename'],
                'defaults':{},
                'func': self.select_command
            },
            'exit': {
                'args': [],
                'defaults': {},
                'func': self.exit_command
            },
            'bye': {
                'args': [],
                'defaults': {},
                'func': self.exit_command
            },
            'quit': {
                'args': [],
                'defaults': {},
                'func': self.exit_command
            }
        }

    def create_command(self, args):
        view.creation_file_prog()
        new_filename = args.get('filename')
        notes_controller.create_file(new_filename)
        utils.continue_loop()

    def listfiles_command(self, args):
        view.listfiles_prog()
        notes_controller.listfiles_render()
        utils.continue_loop()

    def select_command(self, args):
        view.select_file_prog()
        filename = args.get('filename')
        notes_controller.select_filename(filename)
        utils.continue_loop()


    def exit_command(self, args):
        utils.render('', 'bye', 'see you soon', ':)', idx_0 = models.current_style, idx_1 = models.custom_1_style, idx_2 = models.custom_2_style)
        exit(0)

    def parse(self, enter_input):
        parse_input = str(enter_input)
        parts = parse_input.split()
        if parts:
            cmd = parts[0]
            args_provided = parts[1:]
            if cmd in self.commands:
                cmd_info = self.commands[cmd]
                cmd_args = {cmd_info['args'][i]: args_provided[i] for i in range(
                    min(len(cmd_info['args']), len(args_provided)))}
                combined_args = {**cmd_info['defaults'], **cmd_args}
                # Afficher les informations sous forme de dictionnaire
                cmd_info['func'](combined_args)
            else:
                print(f"Commande inconnue : {cmd}")
        else:
            print("Aucune commande entrée")

    def run(self) -> None:
        view.init_program()
        while True:
            view.mainMenu()
            c = utils.enter('', 'your command', '->', idx_0= models.custom_1_style, idx_1 = models.custom_3_style)
            self.parse(c)
