import os
import json

class File:
    def __init__(self):
        self.dir_ = self.connect_dir()

    def connect_dir(self):
        dir_racine = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'notes')
        if not os.path.exists(dir_racine):
            try:
                os.makedirs(dir_racine)
            except Exception as e:
                print(f"Erreur lors de la création du dossier : {e}")
                return None
        return dir_racine

    def write_json(self, filename, content):
        path = os.path.join(self.dir_, filename)
        try:
            with open(path, 'w') as file:
                json.dump(content, file, indent=4)
            return True
        except Exception as e:
            print(f"Erreur lors de l'écriture du fichier JSON : {e}")
            return False

    def read_json(self, filename):
        path = os.path.join(self.dir_, filename)
        try:
            with open(path, 'r') as file:
                return json.load(file)
        except Exception as e:
            print(f"Erreur lors de la lecture du fichier JSON : {e}")
            return None

    def delete_json(self, filename):
        path = os.path.join(self.dir_, filename)
        try:
            os.remove(path)
            return True
        except Exception as e:
            print(f"Erreur lors de la suppression du fichier JSON : {e}")
            return False

    def update_json(self, filename, new_content):
        return self.write_json(filename, new_content)
    
    def list_notes(self):
        return os.listdir(self.dir_)


