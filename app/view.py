import app.utils as utils
import app.models as models
import time






def init_program():
    utils.clear()
    timestamp = 0.5
    utils.render('', 'loading', '...', idx_0= models.default_style_prog, idx_1=models.current_style)
    utils.progress_bar(sleep_time=0.1)
    time.sleep(timestamp)
    utils.clear()
    utils.render('', 'success login', '->','Welcome to your dashboard ;)', 
                idx_0=models.succes_style, 
                idx_2= models.custom_1_style)

def mainMenu():
    header_menu = ['Description', 'Action']
    tables_menu = [
        ['create file', 'create <filename>'],
        ['get all files', 'listfiles <offset>'],
        ['select file', 'select <filename>'],
        ['You can leave this programm', 'exit / bye / quit']
    ]
    utils.grid(header_menu, tables_menu)

def decorator_func(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)  # Appel de la fonction décorée
        utils.progress_bar(sleep_time=0.1)
        utils.clear()
        return result
    return wrapper


@decorator_func
def creation_file_prog():
    text = 'creation file in excecution'
    utils.render('', text, idx_0= models.custom_1_style)

@decorator_func
def listfiles_prog():
    text = 'listfiles in execution'
    utils.render('', text, idx_0=models.custom_1_style)

@decorator_func
def select_file_prog():
    text = 'select filename in execution'
    utils.render('', text,idx_0=models.custom_1_style)