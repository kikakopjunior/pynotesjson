import app.utils as utils



default_style_prog = {
    'color': 'white',
    'background': 'none',
    'style': 'normal' 
}

current_style = {
    'color': 'white',
    'background': 'none',
    'style': 'bright'
}

custom_2_style = {
    'color': 'white_light',
    'background': 'none',
    'style': 'bright'
}

custom_3_style = {
    'color': 'black_light',
    'background': 'none',
    'style': 'bright'
}


succes_style = {
    'color': 'green',
    'background': 'none',
    'style': 'bright'
}

warning_style = {
    'color': 'yellow',
    'background': 'none',
    'style': 'bright'
}

error_style = {
    'color': 'red',
    'background': 'none',
    'style': 'bright'
}

custom_1_style = {
    'color': 'blue',
    'background': 'none',
    'style': 'bright'
}