from app.files import File
import app.utils as utils
import app.models as models


def __creation_file(filename: str, formData: dict):
    file = File()
    c = file.write_json(filename, formData)
    return c


def __update_file(filename: str, data: dict):
    file = File()
    c = file.update_json(filename, data)
    return c


def __list_file():
    file = File()
    lists_files = file.list_notes()
    return lists_files


def _show_listfiles(list_files):
    style_file = {
        'color': 'white_light',
        'background': 'none',
        'style': 'bright'
    }
    for file in list_files:
        utils.render('|',
                     '->', str(file),
                     idx_0={'color': 'red', 'style': 'bright'},
                     idx_1=style_file)


def __loop_write() -> str:
    idx = 0
    text_render = ""
    while True:
        c = utils.enter('', f'{str(idx)}', '->',
                        idx_0=models.current_style,
                        idx_1=models.custom_1_style)
        if c and c not in ['exit', 'quit']:
            if c.endswith('-/-'):
                transform_split = c.split('-/-')[0]
                text_render += transform_split + '\n'
            else:
                text_render += c
        elif c in ['exit', 'quit']:
            break
        else:
            pass
        idx += 1
    return text_render


def create_file(filename: str):
    formData = {
        'title': '',
        'content': ''
    }

    title_input = utils.enter('',
                              'title', '->',
                              idx_0=models.custom_1_style,
                              idx_1=models.custom_2_style
                              )
    content_input = __loop_write()
    formData['title'] = title_input
    formData['content'] = content_input
    check = __creation_file(filename, formData)
    if check:
        print("is ok")
    else:
        print('is not ok')


def listfiles_render():
    lst_files = __list_file()
    _show_listfiles(lst_files)


def __select_custom(filename: str):
    file = File()
    file_target = file.read_json(filename)
    return file_target


def _show_file_target(filename: str):
    file_target = __select_custom(filename)
    title = file_target.get('title') if file_target.get('title') else None
    content = file_target.get(
        'content') if file_target.get('content') else None

    if title and content is None:
        utils.render('', 'Error target file', idx_0=models.error_style)
    else:
        utils.render('title',
                     '->', title,
                     idx_0=models.current_style)

        content = content.split('\n')
        for idx, line in enumerate(content):
            utils.render(str(idx),
                         '->', line,
                         idx_0=models.current_style)


def select_filename(filename: str):
    if filename in __list_file():
        target_filename = __select_custom(filename)
        if target_filename:
            _show_file_target(filename)
        else:
            print('error', target_filename)
    else:
        print('error select file')
